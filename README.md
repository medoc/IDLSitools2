![](http://github.com/SITools2/core-v2/raw/dev/workspace/client-public/res/images/logo_01_petiteTaille.png)
# IDLSitools2_1.0

## Description

IDLSitools2_1.0 is a generic IDL Sitools2V1.0 client

IDLSitools2 tool has been designed by MEDOC at IAS (Institut d'Astrophysique Spatiale) to perform all operations available within Sitools2 via IDL.

The IDLSitools2 routines allow you to query a Sitools2 server, especially for the MEDIA and GAIA-DEM data sets.

## Getting the source code

Download the last archive file using the Download button above, or clone the repository from the URL given by the Clone button, e.g.:
```sh
git clone https://git.ias.u-psud.fr/medoc/IDLSitools2.git IDLSitools2_1.0
```

The retrieved module structure is the following:
```sh
├── clients
│   ├── GAIA-DEM
│   └── MEDIA
├── core
└── example
```

## Installing the module

* Get the source code (see above)
* Add the install directory to your `IDL_PATH` environment variable (example with a bash-like shell):
```sh
export IDL_PATH=$IDL_PATH:+/usr/local/Sitools2Client/
```

## Features

* Do a search by date range, and optionnally a wavelength and a cadence.
* Filter the results with specific keyword values (e.g. filter on quality, cdelt...)
* Download the files corresponding to the search results.

## Examples of use

### MEDIA

* Make a request using the `media_search()` function.
```fortran
sdo_list = media_search (DATES=LIST('2011-01-01T00:00:00', '2011-01-01T00:05:00'), WAVES=LIST('304','193'), CADENCE='1 min', NB_RES_MAX=10)
```
* Download the result of your previous search.
```fortran
media_execute = media_get(MEDIA_DATA_LIST=sdo_list)
```
* Have additional metadata information about each previous answer.
```fortran
for item in sdo_data_list:
    my_meta_search = item.metadata_search(KEYWORDS=LIST('quality', 'cdelt1', 'cdelt2'))
    print my_meta_search
```
* Filter on a specific keyword before downloading data.
```fortran
FOREACH sdo_item, sdo_list DO BEGIN
    meta_data_search = sdo_item->metadata_search(KEYWORDS=LIST('quality', 'cdelt1', 'cdelt2'))
    PRINT, JSON_SERIALIZE(meta_data_search)
    file=obj_new()
    IF meta_data_search['quality'] EQ 0 THEN  file = sdo_item->get_file(TARGET_DIR='/tmp')
ENDFOREACH
```
  
### GAIA-DEM

* Make a request using the gaia_search() function.
```fortran
gaia_list = gaia_search(DATES=LIST('2011-01-01T00:00', '2011-01-01T23:05'),  NB_RES_MAX=10)
```
* Download the result of your previous search.
```fortran
gaia_execute=gaia_get( GAIA_LIST=gaia_list,TARGET_DIR='/tmp' )
```
* Specify the TYPE you want to retrieve, it should be a list with elements among 'temp', 'em', 'width', and 'chi2'.
```fortran
gaia_collect_em_files = gaia_get(GAIA_LIST=gaia_list, TYPE=LIST("em","temp"), target_dir="/tmp")
```
* Download a tar ball file (slower):
```fortran
gaia_tar_collect = gaia_get(GAIA_LIST=gaia_list, DOWNLOAD_TYPE="tar", target_dir="/tmp" , FILENAME="my_download_file.tar")
```

### Requirements

This IDL module requires IDL 8.2 or above.
